#ifndef CONVERTER_H
#define CONVERTER_H

#include <string>
#include <iostream>
#include <vector>

struct Converter
{
    Converter();

    static const int MAX_VECTOR_HEIGHT;

    static std::vector<std::vector<std::string>> finalArt;

    static void generateArt(std::string input);
    static std::vector<std::string> charToASCIIChar(char character);
    static void printArt();
};

#endif // CONVERTER_H
