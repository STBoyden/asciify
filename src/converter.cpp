#include "headers/converter.h"

Converter::Converter() {}

const int Converter::MAX_VECTOR_HEIGHT = 6;

std::vector<std::vector<std::string>> Converter::finalArt = {};

void Converter::generateArt(std::string input)
{
  for (unsigned int i = 0; i < input.size(); i++)
    finalArt.push_back(charToASCIIChar(input[i]));
}

std::vector<std::string> Converter::charToASCIIChar(char character)
{
  std::vector<std::string> characterVector;

  switch (character) {
  case ' ':
    characterVector = {
      R"(   )",
      R"(   )",
      R"(   )",
      R"(   )",
      R"(   )",
      R"(   )"
    };
    break;
  case 'a':
  case 'A':
    characterVector = {
      R"(          )",
      R"(    /\    )",
      R"(   /  \   )",
      R"(  / /\ \  )",
      R"( / ____ \ )",
      R"(/_/    \_\)"
    };
    break;
  case 'b':
  case 'B':
    characterVector = {
      R"( ____  )",
      R"(|  _ \ )",
      R"(| |_) |)",
      R"(|  _ < )",
      R"(| |_) |)",
      R"(|____/ )"
    };
    break;
  case 'c':
  case 'C':
    characterVector = {
      R"(  _____ )",
      R"( / ____|)",
      R"(| |     )",
      R"(| |     )",
      R"(| |____ )",
      R"( \_____|)"
    };
    break;
  case 'd':
  case 'D':
    characterVector = {
      R"( _____  )",
      R"(|  __ \ )",
      R"(| |  | |)",
      R"(| |  | |)",
      R"(| |__| |)",
      R"(|_____/ )"
    };
    break;
  case 'e':
  case 'E':
    characterVector = {
      R"( ______ )",
      R"(|  ____|)",
      R"(| |__   )",
      R"(|  __|  )",
      R"(| |____ )",
      R"(|______|)"
    };
    break;
  case 'f':
  case 'F':
    characterVector = {
      R"( ______ )",
      R"(|  ____|)",
      R"(| |__   )",
      R"(|  __|  )",
      R"(| |     )",
      R"(|_|     )"
    };
    break;
  case 'g':
  case 'G':
    characterVector = {
      R"(  _____ )",
      R"( / ____|)",
      R"(| |  __ )",
      R"(| | |_ |)",
      R"(| |__| |)",
      R"( \_____|)"
    };
    break;
  case 'h':
  case 'H':
    characterVector = {
      R"( _    _ )",
      R"(| |  | |)",
      R"(| |__| |)",
      R"(|  __  |)",
      R"(| |  | |)",
      R"(|_|  |_|)"
    };
    break;
  case 'i':
  case 'I':
    characterVector = {
      R"( _____ )",
      R"(|_   _|)",
      R"(  | |  )",
      R"(  | |  )",
      R"( _| |_ )",
      R"(|_____|)"
    };
    break;
  case 'j':
  case 'J':
    characterVector = {
      R"(      _ )",
      R"(     | |)",
      R"(     | |)",
      R"( _   | |)",
      R"(| |__| |)",
      R"( \____/ )"
    };
    break;
  case 'k':
  case 'K':
    characterVector = {
      R"( _  __)",
      R"(| |/ /)",
      R"(| ' / )",
      R"(|  <  )",
      R"(| . \ )",
      R"(|_|\_\)"
    };
    break;
  case 'l':
  case 'L':
    characterVector = {
      R"( _      )",
      R"(| |     )",
      R"(| |     )",
      R"(| |     )",
      R"(| |____ )",
      R"(|______|)"
    };
    break;
  case 'm':
  case 'M':
    characterVector = {
      R"( __  __ )",
      R"(|  \/  |)",
      R"(| \  / |)",
      R"(| |\/| |)",
      R"(| |  | |)",
      R"(|_|  |_|)"
    };
    break;
  case 'n':
  case 'N':
    characterVector = {
      R"( _   _ )",
      R"(| \ | |)",
      R"(|  \| |)",
      R"(| . ` |)",
      R"(| |\  |)",
      R"(|_| \_|)"
    };
    break;
  case 'o':
  case 'O':
    characterVector = {
      R"(  ____  )",
      R"( / __ \ )",
      R"(| |  | |)",
      R"(| |  | |)",
      R"(| |__| |)",
      R"( \____/ )"
    };
    break;
  case 'p':
  case 'P':
    characterVector = {
      R"( _____  )",
      R"(|  __ \ )",
      R"(| |__) |)",
      R"(|  ___/ )",
      R"(| |     )",
      R"(|_|     )"
    };
    break;
  case 'q':
  case 'Q':
    characterVector = {
      R"(  ____  )",
      R"( / __ \ )",
      R"(| |  | |)",
      R"(| |  | |)",
      R"(| |__| |)",
      R"( \___\_\)"
    };
    break;
  case 'r':
  case 'R':
    characterVector = {
      R"( _____  )",
      R"(|  __ \ )",
      R"(| |__) |)",
      R"(|  _  / )",
      R"(| | \ \ )",
      R"(|_|  \_\)"
    };
    break;
  case 's':
  case 'S':
    characterVector = {
      R"(  _____ )",
      R"( / ____|)",
      R"(| (___  )",
      R"( \___ \ )",
      R"( ____) |)",
      R"(|_____/ )"
    };
    break;
  case 't':
  case 'T':
    characterVector = {
      R"( _______ )",
      R"(|__   __|)",
      R"(   | |   )",
      R"(   | |   )",
      R"(   | |   )",
      R"(   |_|   )"
    };
    break;
  case 'u':
  case 'U':
    characterVector = {
      R"( _    _ )",
      R"(| |  | |)",
      R"(| |  | |)",
      R"(| |  | |)",
      R"(| |__| |)",
      R"( \____/ )"
    };
    break;
  case 'v':
  case 'V':
    characterVector = {
      R"(__      __)",
      R"(\ \    / /)",
      R"( \ \  / / )",
      R"(  \ \/ /  )",
      R"(   \  /   )",
      R"(    \/    )"
    };
    break;
  case 'w':
  case 'W':
    characterVector = {
      R"(__          __)",
      R"(\ \        / /)",
      R"( \ \  /\  / / )",
      R"(  \ \/  \/ /  )",
      R"(   \  /\  /   )",
      R"(    \/  \/    )"
    };
    break;
  case 'x':
  case 'X':
    characterVector = {
      R"(__   __)",
      R"(\ \ / /)",
      R"( \ V / )",
      R"(  > <  )",
      R"( / . \ )",
      R"(/_/ \_\)"
    };
    break;
  case 'y':
  case 'Y':
    characterVector = {
      R"(__     __)",
      R"(\ \   / /)",
      R"( \ \_/ / )",
      R"(  \   /  )",
      R"(   | |   )",
      R"(   |_|   )"
    };
    break;
  case 'z':
  case 'Z':
    characterVector = {
      R"( ______)",
      R"(|___  /)",
      R"(   / / )",
      R"(  / /  )",
      R"( / /__ )",
      R"(/_____|)"
    };
    break;
    /// TODO: Append the remaining uppercase characters, lowercase characters,
    /// appropriate symbols and punctuation.
    break;
  default:
    characterVector = { "", "", "", "", "", "" };
  }

  return characterVector;
}

void Converter::printArt()
{
  for (auto y = 0; y < MAX_VECTOR_HEIGHT; y++) {
    for (auto x = 0; x < int(finalArt.size()); x++)
      std::cout << finalArt[x][y];
    std::cout << std::endl;
  }
}
