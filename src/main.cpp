#include <iostream>
#include <string>

#include "headers/converter.h"

int main(int argc, char* argv[])
{
  std::string str;

  if (argc != 2 || argc > 2) {
    std::cout << "Invalid usage" << std::endl;
    std::cout << "Usage: asciify [OPTIONS] <string>" << std::endl;
    exit(1);
  }

  str.insert(0, argv[1]);

  Converter::generateArt(str);

  Converter::printArt();

  std::cout << "\n" << std::endl;
}
